#ifndef NCRL_OFFB_VIO_H
#define NCRL_OFFB_VIO_H
#include <cmath>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
// dji include
#include <dji_sdk/dji_sdk.h>
#include <dji_sdk/dji_sdk_node.h>

#define GRAVITY 9.81

ros::ServiceClient sdk_ctrl_authority_service;
ros::ServiceClient drone_task_service;
ros::ServiceClient query_version_service;

uint8_t flight_status = 255;

// declare flight control flag
// VERTICAL_THRUST = 0~100%
// HORIZONTAL_ANGLE = limit 0.611 rad
// YAW_ANGLE = limit -pi to pi
// body frame
uint8_t flag = (
                DJISDK::VERTICAL_THRUST      |
                DJISDK::HORIZONTAL_ANGLE     |
                DJISDK::YAW_ANGLE            |
                DJISDK::HORIZONTAL_BODY      |
                DJISDK::STABLE_ENABLE
               );

struct XYZyaw{
  float x; float y; float z; float yaw;
};

struct PID{
  float KP; float KI; float KD;
  float PR; float IN; float DE;
};

struct trajectory{
  float px; float vx; float ax;
  float py; float vy; float ay;
};

char getch(){
    int flags = fcntl(0, F_GETFL, 0);
    fcntl(0, F_SETFL, flags | O_NONBLOCK);

    char buf = 0;
    struct termios old = {0};
    if (tcgetattr(0, &old) < 0) {
        perror("tcsetattr()");
    }
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0) {
        perror("tcsetattr ICANON");
    }
    if (read(0, &buf, 1) < 0) {
        //perror ("read()");
    }
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0) {
        perror ("tcsetattr ~ICANON");
    }
    return (buf);
}

void confine(float &data, float threshold)
{
  if(data >= threshold || data <= -threshold){
    if(data >= threshold)
      data = threshold;
    else
      data = -threshold;
  }
  else{
    data = data;
  }
}

bool obtain_control()
{
  dji_sdk::SDKControlAuthority authority;
  authority.request.control_enable=1;
  sdk_ctrl_authority_service.call(authority);

  if(!authority.response.result)
  {
    ROS_ERROR("obtain control failed!");
    return false;
  }

  return true;
}

bool takeoff_land(int task)
{
  dji_sdk::DroneTaskControl droneTaskControl;

  droneTaskControl.request.task = task;

  drone_task_service.call(droneTaskControl);

  if(!droneTaskControl.response.result)
  {
    ROS_ERROR("takeoff_land fail");
    return false;
  }

  return true;
}

bool is_M100()
{
  dji_sdk::QueryDroneVersion query;
  query_version_service.call(query);

  if(query.response.version == DJISDK::DroneFirmwareVersion::M100_31)
  {
    return true;
  }
  return false;
}

bool M100monitoredTakeoff()
{
  ros::Time start_time = ros::Time::now();

  if(!takeoff_land(dji_sdk::DroneTaskControl::Request::TASK_TAKEOFF)){
    return false;
  }

  ros::Duration(0.01).sleep();
  ros::spinOnce();

  // Step 1: If M100 is not in the air after 10 seconds, fail.
  while (ros::Time::now() - start_time < ros::Duration(10)){
    ros::Duration(0.01).sleep();
    ros::spinOnce();
  }

  if(flight_status != DJISDK::M100FlightStatus::M100_STATUS_IN_AIR)
  {
    ROS_ERROR("Takeoff failed.");
    return false;
  }
  else
  {
    start_time = ros::Time::now();
    ROS_INFO("Successful takeoff!");
    ros::spinOnce();
  }
  return true;
}

void rotation(float &x, float &y, float &z, double rx, double ry, double rz)
{
  float x1 = x;
  float y1 = cos(rx) * y - sin(rx) * z;
  float z1 = sin(rx) * y + cos(rx) * z;

  float x2 = sin(ry)*z1 + cos(ry)*x1;
  float y2 = y1;
  float z2 = cos(ry)*z1 - sin(ry)*x1;

  x = cos(rz)*x2 - sin(rz)*y2;
  y = sin(rz)*x2 + cos(rz)*y2;
  z = z2;
}

void rotZ(float &x,float &y, double rz)
{
  float x1 = x;
  float y1 = y;
  x = cos(rz) * x1 - sin(rz) * y1;
  y = sin(rz) * x1 + cos(rz) * y1;
}
#endif // NCRL_OFFB_VIO_H
