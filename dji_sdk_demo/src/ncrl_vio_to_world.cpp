#include <ros/ros.h>
#include <dji_sdk_demo/ncrl_offb_vio.h>

ros::Subscriber sub_vio;
ros::Publisher pub_vio;

// declare msg
nav_msgs::Odometry vio_i_camera;
nav_msgs::Odometry vio_w_body;

tf::Quaternion W_to_I;
tf::Quaternion I_to_C;
tf::Quaternion C_to_B;
tf::Quaternion temp;
tf::Quaternion W_to_B;

struct XYZyaw pose;
struct XYZyaw bias;
struct XYZyaw velo;

double roll, pitch, yaw;

bool init = true;
bool receive = false;

void vio_cb(const nav_msgs::Odometry::ConstPtr& msg)
{
  vio_i_camera = *msg;
  vio_w_body = vio_i_camera;
  // handle pose and velocity
  pose.x = vio_i_camera.pose.pose.position.x;
  pose.y = vio_i_camera.pose.pose.position.y;
  pose.z = vio_i_camera.pose.pose.position.z;

  velo.x = vio_i_camera.twist.twist.linear.x;
  velo.y = vio_i_camera.twist.twist.linear.y;
  velo.z = vio_i_camera.twist.twist.linear.z;

  if(init){
    init = false;
    bias.x = pose.x;
    bias.y = pose.y;
    bias.z = pose.z;
  }
  else{
    pose.x = pose.x - bias.x;
    pose.y = pose.y - bias.y;
    pose.z = pose.z - bias.z;
  }

  W_to_I.setRPY(0, 0, -M_PI_2);
  tf::quaternionMsgToTF(vio_i_camera.pose.pose.orientation, I_to_C);
  temp.setRPY(M_PI_2, 0, 0);
  C_to_B.setRPY(0, 0, M_PI_2);
  W_to_B = W_to_I * I_to_C * temp * C_to_B;
  tf::Matrix3x3(W_to_B).getRPY(roll, pitch, yaw);

  printf("Euler angle : %f \t %f \t %f\n",roll,pitch,yaw);
  // rotate to world frame
  rotation(pose.x, pose.y, pose.z, 0, 0, -0.5*M_PI);
  rotation(velo.x, velo.y, velo.z, 0, 0, -0.5*M_PI);

  vio_w_body.pose.pose.position.x = pose.x;
  vio_w_body.pose.pose.position.y = pose.y;
  vio_w_body.pose.pose.position.z = pose.z;

  vio_w_body.twist.twist.linear.x = velo.x;
  vio_w_body.twist.twist.linear.y = velo.y;
  vio_w_body.twist.twist.linear.z = velo.z;

  tf::quaternionTFToMsg(W_to_B,vio_w_body.pose.pose.orientation);
  pub_vio.publish(vio_w_body);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ncrl_vio_to_world");
  ros::NodeHandle nh;
  sub_vio = nh.subscribe<nav_msgs::Odometry> ("vins_estimator/odometry",2, &vio_cb);
  pub_vio = nh.advertise<nav_msgs::Odometry> ("vio_m100",2);

  ros::spin();
}
