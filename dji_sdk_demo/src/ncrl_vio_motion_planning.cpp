/*
 * Target : Using position and velocity from VIO to automatic control M100 motion planning
 */
#include <ros/ros.h>
#include <dji_sdk_demo/ncrl_motion_vio.h>

// declare subscriber
ros::Subscriber sub_flight_status;
ros::Subscriber sub_dji_battery;
ros::Subscriber sub_vio_vel;
// declare publisher
ros::Publisher pub_attitude_ctrl;
// declare test publisher
ros::Publisher pub_motion_position;
ros::Publisher pub_ground_position;
ros::Publisher pub_motion_velocity;
ros::Publisher pub_estima_velocity;

// declare msgs
nav_msgs::Odometry vio_local_position;
sensor_msgs::BatteryState battery;
geometry_msgs::Point goal_pos;
geometry_msgs::Point m100_pos;
geometry_msgs::Point goal_vel;
geometry_msgs::Point esti_vel;

// declare tf
tf::Quaternion InitToVio;
tf::Quaternion VioToBody;
tf::Quaternion WorldToBody;
tf::Quaternion InitToBody;
tf::Quaternion WorldToInit;
tf::Quaternion InitToWorld;

bool init = true;
// declare yaw variables
float goal_yaw = 0;
float err_yaw;
float yaw_cmd;

// world to body
double roll, pitch, yaw;
float roll_cmd, pitch_cmd, thrust_cmd;
float desired_z;
float bias;
float x_in;
float y_in;

// for detect delay
float bias_battery = 0;
int count = 0;
int stamp = 0;
int stamp_last = 0;

// motion
float radius = 0;
int T_cycle = 0;
int motion_state = 3;
int last_state = 0;

// time
double time_now = 0;
double time_start = 0;
double time_temp = 0;
double time_all = 0;
double time_dur = 0;

double time_motion_start = 0;
double time_motion_dur = 0;

double time_part_s = 0;
double time_part_f = 0;
double time_part_e = 0;

// declare detect delay mocap
int delay_a = 0; // delay all
int delay_c = 0; // delay continous
int delay_t = 0; // delay temp
int mocap_delay = 0;
int no_delay = 0;

struct XYZ pose;
struct XYZ goal;
struct XYZ err;
struct XYZ acc; // command
struct XYZ vel; // D term of motion state 2
struct XYZ pose_bias;

struct PID pid_ver;
struct PID pid_yaw;
struct PID pid_x;
struct PID pid_y;
struct PID motion_x;
struct PID motion_y;

struct trajectory motion;

void timeDuration(double &start, double &end, double &dur)
{
  start = ros::Time::now().toSec();
  dur = start - end;
  end = start;
}

void keyboard_control()
{
  int c = getch();
  //ROS_INFO("C: %d",c);
  if (c != EOF) {
    switch (c)
    {
      case 111:     // key up  o
      goal.z += 0.01;
      //ROS_INFO("z+!!!");
      break;
      case 112:     // key down p
      goal.z -= 0.1;
      //ROS_INFO("z-!!!");
      break;
      case 119:     // key foward  w
      goal.x += 0.05;
      break;
      case 115:     // key back   s
      goal.x -= 0.05;
      break;
      case 97:      // key left    a
      goal.y += 0.05;
      break;
      case 100:     // key right   d
      goal.y -= 0.05;
      break;
      case 114:     // key return  r
      goal.x = 0;
      goal.y = 0;
      goal.z = 0;
      goal_yaw = 0;
      break;
      case 105:    // i
      goal_yaw -= 0.1;
      break;
      case 117:    // u
      goal_yaw += 0.1;
      break;
    }
  }
}

void motion_state_control()
{
  int c = getch();
  //ROS_INFO("C: %d",c);
  if (c != EOF) {
    switch (c)
    {
      case 49:     // key 1
      motion_state = 1;
      break;
      case 50:     // key 2
      motion_state = 2;
      break;
      case 51:     // key 3
      motion_state = 3;
      break;
      case 52:     // key 4
      motion_state = 4;
      break;
    }
  }
}

void motion_planning_circle(float r, float T, float theta, struct trajectory &m)
{
  // rotate velocity
  float w = 2*M_PI/T;
  // x axis
  m.px =  r * cos(theta);
  m.vx = -r * sin(theta) * w;
  m.ax = -r * cos(theta) * w * w;
  // y axis
  m.py =  r * sin(theta);
  m.vy =  r * cos(theta) * w;
  m.ay = -r * sin(theta) * w * w;
}

void status_cb(const std_msgs::UInt8::ConstPtr& msg)
{
  flight_status = msg->data;
}

void dji_battery_cb(const sensor_msgs::BatteryState::ConstPtr& msg)
{
  battery = *msg;
}

void vio_cb(const nav_msgs::Odometry::ConstPtr& msg)
{
  vio_local_position = *msg;
  pose.x = vio_local_position.pose.pose.position.x;
  pose.y = vio_local_position.pose.pose.position.y;
  pose.z = vio_local_position.pose.pose.position.z;
  vel.x = vio_local_position.twist.twist.linear.x;
  vel.y = vio_local_position.twist.twist.linear.y;
  vel.z = vio_local_position.twist.twist.linear.z;

  tf::quaternionMsgToTF(vio_local_position.pose.pose.orientation, InitToVio);
  VioToBody.setRPY(0.5*M_PI, 0,0.5*M_PI);
  InitToWorld.setRPY(0, 0, 0.5*M_PI);
  WorldToInit = InitToWorld.inverse();
  WorldToBody = VioToBody * InitToVio * WorldToInit;
  tf::Matrix3x3(WorldToBody).getRPY(roll, pitch, yaw);
  //printf("euler angle : %f \t %f \t %f \n",roll,pitch,yaw);
  // rotate to world frame
  rotation(pose.x, pose.y, pose.z, 0, 0, -0.5*M_PI);
  rotation(vel.x, vel.y, vel.z, 0 ,0 , -0.5*M_PI);

  if(init){
    init = false;
    pose_bias.x = pose.x;
    pose_bias.y = pose.y;
    pose_bias.z = pose.z;
  }
  else{
    pose.x = pose.x - pose_bias.x;
    pose.y = pose.y - pose_bias.y;
    pose.z = pose.z - pose_bias.z;
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ncrl_vio_motion_planning");
  ros::NodeHandle nh;

  // subscriber
  sub_flight_status = nh.subscribe<std_msgs::UInt8> ("dji_sdk/flight_status",2, &status_cb);
  sub_dji_battery   = nh.subscribe<sensor_msgs::BatteryState> ("dji_sdk/battery_state",2, &dji_battery_cb);
  sub_vio_vel       = nh.subscribe<nav_msgs::Odometry> ("vins_estimator/odometry",2, &vio_cb);
  // publisher
  pub_attitude_ctrl = nh.advertise<sensor_msgs::Joy> ("dji_sdk/flight_control_setpoint_generic",2);
  // publisher test
  pub_motion_position = nh.advertise<geometry_msgs::Point> ("motion/position",2);
  pub_ground_position = nh.advertise<geometry_msgs::Point> ("ground/position",2);
  pub_motion_velocity = nh.advertise<geometry_msgs::Point> ("motion/velocity",2);
  pub_estima_velocity = nh.advertise<geometry_msgs::Point> ("estima/velocity",2);

  // service client
  sdk_ctrl_authority_service = nh.serviceClient<dji_sdk::SDKControlAuthority> ("dji_sdk/sdk_control_authority");
  drone_task_service         = nh.serviceClient<dji_sdk::DroneTaskControl>("dji_sdk/drone_task_control");
  query_version_service      = nh.serviceClient<dji_sdk::QueryDroneVersion>("dji_sdk/query_drone_version");

  bool obtain_control_result = obtain_control();
  bool takeoff_result;

  // obtain control then takeoff
  if(obtain_control_result){
    ROS_INFO("obtain control success");
    if(is_M100())
    {
      ROS_INFO("M100 taking off!");
      takeoff_result = M100monitoredTakeoff();
    }
  }
  /////////////////////////////////////////////////////////////////////////////
  // initial
  pid_ver.KP = 0;pid_ver.KI = 0;pid_ver.KD = 0;
  pid_ver.IN = 0;pid_ver.DE = 0;pid_ver.PR = 0;

  pid_yaw.KP = 0;pid_yaw.KI = 0;pid_yaw.KD = 0;
  pid_yaw.IN = 0;pid_yaw.DE = 0;pid_yaw.PR = 0;

  pid_x.KP = 0;pid_x.KI = 0;pid_x.KD = 0;
  pid_x.PR = 0;pid_x.IN = 0;pid_x.DE = 0;

  pid_y.KP = 0;pid_y.KI = 0;pid_y.KD = 0;
  pid_y.PR = 0;pid_y.IN = 0;pid_y.DE = 0;

  motion_x.KP = 0;motion_x.KI = 0;motion_x.KD = 0;
  motion_x.PR = 0;motion_x.IN = 0;motion_x.DE = 0;

  motion_y.KP = 0;motion_y.KI = 0;motion_y.KD = 0;
  motion_y.PR = 0;motion_y.IN = 0;motion_y.DE = 0;

  goal.x = 0;goal.y = 0;goal.z = 0;

  motion.px = 0; motion.vx = 0; motion.ax = 0;
  motion.py = 0; motion.vy = 0; motion.ay = 0;

  // get parameter from launch file
  ros::param::get("~ver_KP",pid_ver.KP);
  ros::param::get("~ver_KI",pid_ver.KI);
  ros::param::get("~ver_KD",pid_ver.KD);

  ros::param::get("~X_KP",pid_x.KP);
  ros::param::get("~X_KI",pid_x.KI);
  ros::param::get("~X_KD",pid_x.KD);

  ros::param::get("~Y_KP",pid_y.KP);
  ros::param::get("~Y_KI",pid_y.KI);
  ros::param::get("~Y_KD",pid_y.KD);

  ros::param::get("~yaw_KP",pid_yaw.KP);
  ros::param::get("~yaw_KI",pid_yaw.KI);
  ros::param::get("~yaw_KD",pid_yaw.KD);

  ros::param::get("~motion_X_KP",motion_x.KP);
  ros::param::get("~motion_X_KI",motion_x.KI);
  ros::param::get("~motion_X_KD",motion_x.KD);

  ros::param::get("~motion_Y_KP",motion_y.KP);
  ros::param::get("~motion_Y_KI",motion_y.KI);
  ros::param::get("~motion_Y_KD",motion_y.KD);

  ros::param::get("~desired_height",desired_z);
  ros::param::get("~bias",bias);
  ros::param::get("~mocap_delay",mocap_delay);

  ros::param::get("~radius",radius);
  ros::param::get("~T_cycle",T_cycle);

  ros::Rate r(100);
  while(ros::ok()){
    if(obtain_control_result && takeoff_result){
      // time calculate
      timeDuration(time_part_s,time_part_f,time_part_e);
      printf("time start loop : %f\n",time_part_e);

      // obtain control
      if(obtain_control_result == obtain_control()){
        obtain_control_result = true;
      }
      else{
        obtain_control_result = false;
        ROS_INFO("Prepare to landing");
        takeoff_land(dji_sdk::DroneTaskControl::Request::TASK_LAND);
        break;
      }

      // time compute
      time_now = ros::Time::now().toSec();
      if (count == 0){
        time_start  = time_now;
      }
      else{
        time_dur  = time_now - time_temp;
        time_all  = time_now - time_start;
        printf("time : %f \t all : %f sec\n",time_dur,time_all);
      }
      time_temp = time_now;

//      // safety checking (mocap delay)
//      if (stamp_last == stamp){
//        ROS_WARN("MOCAP DELAY");
//        no_delay = 0;
//        delay_a++;
//        delay_t++;
//        if (delay_t > delay_c){
//          delay_c = delay_t;
//        }
//      }
//      else{
//        no_delay++;
//        if (no_delay >= 10){
//          delay_t = 0;
//        }
//      }

//      printf("total delay : %d\n",delay_a);
//      printf("continous delay : %d\n",delay_c);
//      if (count <= 500 || time_all <= 5){
//        if (delay_c >= 4){
//          float delay_time = time_all;
//          ROS_WARN("dalay exceed %f sec",delay_time);
//          ROS_INFO("Prepare to landing");
//          takeoff_land(dji_sdk::DroneTaskControl::Request::TASK_LAND);
//          break;
//        }
//      }
//      else{
//        if (delay_c >= mocap_delay){
//          float delay_time = mocap_delay;
//          delay_time = delay_time/100;
//          ROS_WARN("dalay exceed %f sec",delay_time);
//          ROS_INFO("Prepare to landing");
//          takeoff_land(dji_sdk::DroneTaskControl::Request::TASK_LAND);
//          break;
//        }
//      }
//      stamp_last = stamp;

      motion_state_control();
      printf("motion state : %d\n",motion_state);
      // printf message to debug
      printf("World pos : %f \t %f \t %f \n",pose.x,pose.y,pose.z);
      printf("bias pos : %f \t %f \t %f \n",pose_bias.x,pose_bias.y,pose_bias.z);
      printf("battery : %f\n",battery.percentage);
//      timeDuration(time_part_s,time_part_f,time_part_e);
//      printf("time before motion state : %f\n",time_part_e);
////////////////////////////////////////////////////////////////////////////////////////////////////////
      if (motion_state == 1){

        goal.x = radius;
        goal.y = 0;

        err.x = goal.x - pose.x;
        err.y = goal.y - pose.y;
        err.z = goal.z + desired_z - pose.z;
        err_yaw = goal_yaw - yaw;

        //printf("goal(in) x : %f \t y : %f \t z : %f \t yaw : %f\n",goal.x, goal.y, goal.z+desired_z, goal_yaw);
        //printf("err(b) x : %f \t y : %f \t z : %f \t yaw : %f\n",err.x,err.y,err.z,err_yaw);

        float temp = pid_x.PR;
        pid_x.PR = err.x;
        pid_x.IN += err.x;
        pid_x.DE = (err.x - temp)/time_dur;
        confine(pid_x.IN,20);
        x_in = pid_x.IN;

        temp = pid_y.PR;
        pid_y.PR = err.y;
        pid_y.IN += err.y;
        pid_y.DE = (err.y - temp)/time_dur;
        confine(pid_y.IN,20);
        y_in = pid_y.IN;

        // inertia frame
        acc.x = ( pid_x.PR * pid_x.KP
                + pid_x.IN * pid_x.KI
                + pid_x.DE * pid_x.KD * time_dur);

        acc.y = ( pid_y.PR * pid_y.KP
                + pid_y.IN * pid_y.KI
                + pid_y.DE * pid_y.KD * time_dur);

        rotZ(acc.x, acc.y, -yaw);

//        timeDuration(time_part_s,time_part_f,time_part_e);
//        printf("time motion state 1 end : %f\n",time_part_e);
      }
////////////////////////////////////////////////////////////////////////////////////////////////////////
      else if (motion_state == 2){
        // check whether start of motion planning
        if (last_state != motion_state){
          time_motion_start = time_now;
          time_motion_dur = 0;
        }

        // caulate time of motion
        time_motion_dur += time_dur;
        if (time_motion_dur >= T_cycle){
          time_motion_dur -= T_cycle;
        }

        float CurrOri = atan2(pose.y,pose.x) + M_PI;
        float GoalOri = 2*M_PI*time_motion_dur/T_cycle;
        float xydis = sqrt(pose.x * pose.x + pose.y * pose.y);
        motion_planning_circle(radius, T_cycle, GoalOri, motion);

        printf("err theta : %f\n",RAD2DEG(GoalOri - CurrOri));
        printf("err dista : %f\n",xydis - radius);

        // for debug
        goal_pos.x = motion.px;
        goal_pos.y = motion.py;
        pub_motion_position.publish(goal_pos);

        m100_pos.x = pose.x;
        m100_pos.y = pose.y;
        pub_ground_position.publish(m100_pos);

        goal_vel.x = motion.vx;
        goal_vel.y = motion.vy;
        pub_motion_velocity.publish(goal_vel);

        esti_vel.x = vel.x;
        esti_vel.y = vel.y;
        pub_estima_velocity.publish(esti_vel);

        err.x = motion.px - pose.x;
        err.y = motion.py - pose.y;
        err.z = goal.z + desired_z - pose.z;
        err_yaw = goal_yaw - yaw;

        vel.x = motion.vx - vel.x;
        vel.y = motion.vy - vel.y;

        motion_x.PR = err.x;
        motion_x.IN = x_in;
        motion_x.DE = vel.x * time_dur;

        motion_y.PR = err.y;
        motion_y.IN = y_in;
        motion_y.DE = vel.y * time_dur;

        // body frame
        acc.x = ( motion_x.PR * motion_x.KP
                + motion_x.IN * motion_x.KI
                + motion_x.DE * motion_x.KD ) + motion.ax;

        acc.y = ( motion_y.PR * motion_y.KP
                + motion_y.IN * motion_y.KI
                + motion_y.DE * motion_y.KD ) + motion.ay;

        // trans to boy frame which z is aligned with global z
        rotZ(acc.x,acc.y,-yaw);

//        timeDuration(time_part_s,time_part_f,time_part_e);
//        printf("time motion state 2 end : %f\n",time_part_e);
      }
////////////////////////////////////////////////////////////////////////////////////////////////////////
      else if (motion_state == 3){
        // check whether start of motion planning
        if (last_state != motion_state){
          goal.x = pose.x;
          goal.y = pose.y;
          pid_x.IN = 0;
          pid_y.IN = 0;
        }
        else{
          keyboard_control();
        }
        err.x = goal.x - pose.x;
        err.y = goal.y - pose.y;
        err.z = goal.z + desired_z - pose.z;
        err_yaw = goal_yaw - yaw;

        //printf("goal(in) x : %f \t y : %f \t z : %f \t yaw : %f\n",goal.x, goal.y, goal.z+desired_z, goal_yaw);
        //printf("err(b) x : %f \t y : %f \t z : %f \t yaw : %f\n",err.x,err.y,err.z,err_yaw);

        float temp = pid_x.PR;
        pid_x.PR = err.x;
        pid_x.IN += err.x;
        pid_x.DE = (err.x - temp)/time_dur;
        confine(pid_x.IN,20);

        temp = pid_y.PR;
        pid_y.PR = err.y;
        pid_y.IN += err.y;
        pid_y.DE = (err.y - temp)/time_dur;
        confine(pid_y.IN,20);

        // body frame
        acc.x = ( pid_x.PR * pid_x.KP
                + pid_x.IN * pid_x.KI
                + pid_x.DE * pid_x.KD * time_dur);

        acc.y = ( pid_y.PR * pid_y.KP
                + pid_y.IN * pid_y.KI
                + pid_y.DE * pid_y.KD * time_dur);

        rotZ(acc.x, acc.y, -yaw);

//        timeDuration(time_part_s,time_part_f,time_part_e);
//        printf("time motion state 3 end : %f\n",time_part_e);
      }
////////////////////////////////////////////////////////////////////////////////////////////////////////
      else{
        ROS_INFO("Prepare to landing");
        takeoff_land(dji_sdk::DroneTaskControl::Request::TASK_LAND);
        break;
      }
      float temp = pid_ver.PR;
      pid_ver.PR = err.z;
      pid_ver.IN += err.z;
      pid_ver.DE = err.z - temp;
//      printf("vertical PR : %f \t IN : %f \t DE : %f\n",pid_ver.PR,pid_ver.IN,pid_ver.DE);
      confine(pid_ver.IN,30);

      temp = pid_yaw.PR;
      pid_yaw.PR = err_yaw;
      pid_yaw.IN += err_yaw;
      pid_yaw.DE = err_yaw - temp;
      confine(pid_yaw.IN,1);

      thrust_cmd = ( pid_ver.PR * pid_ver.KP
                   + pid_ver.IN * pid_ver.KI
                   + pid_ver.DE * pid_ver.KD) + bias;

      if (count <= 500 || time_all <= 5){
        confine(thrust_cmd,80);
      }

      yaw_cmd = ( pid_yaw.PR * pid_yaw.KP
                + pid_yaw.IN * pid_yaw.KI
                + pid_yaw.DE * pid_yaw.KD);

      // handle desire roll and pitch
      roll_cmd = -atan2(acc.y,GRAVITY);
      pitch_cmd = atan2(acc.x,GRAVITY);
      confine(roll_cmd,0.5);
      confine(pitch_cmd,0.5);

      last_state = motion_state;

      printf("cmd : %f \t %f \t %f \t %f \n",roll_cmd,pitch_cmd,thrust_cmd,yaw_cmd);
      // publish cmd
      sensor_msgs::Joy controldata;
      controldata.axes.push_back(roll_cmd);
      controldata.axes.push_back(pitch_cmd);
      controldata.axes.push_back(thrust_cmd);
      controldata.axes.push_back(yaw_cmd);
      controldata.axes.push_back(flag);
      pub_attitude_ctrl.publish(controldata);
      count ++;

      printf("euler : %f \t %f \t %f \n",roll,pitch,yaw);
      printf("err : %f \t %f \t %f \n",err.x,err.y,err.z);
//      timeDuration(time_part_s,time_part_f,time_part_e);
//      printf("time end loop : %f\n",time_part_e);
      printf("----------------------------------------------\n");
    }
    ros::spinOnce();
    r.sleep();
  }
}
