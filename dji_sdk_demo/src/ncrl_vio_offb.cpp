#include <ros/ros.h>
#include <dji_sdk_demo/ncrl_offb_vio.h>

// declare subscriber
ros::Subscriber sub_flight_status;
ros::Subscriber sub_dji_battery;
ros::Subscriber sub_vio;
ros::Subscriber sub_host_mocap;
// declare publisher
ros::Publisher pub_attitude_ctrl;
ros::Publisher pub_world_position;

// declare msgs
nav_msgs::Odometry vio_msgs;
sensor_msgs::BatteryState battery;
geometry_msgs::Point world_pos;
geometry_msgs::PoseStamped mocap_pos;
// declare tf
tf::Quaternion InitToVio;
tf::Quaternion VioToBody;
tf::Quaternion WorldToBody;
tf::Quaternion InitToBody;
tf::Quaternion WorldToInit;
tf::Quaternion InitToWorld;

bool init = true;
bool obtain_control_result = false;
bool takeoff_result = false;

// declare m100 cmd
float yaw_cmd = 0;
float roll_cmd = 0;
float pitch_cmd = 0;
float thrust_cmd = 0;

// declare euler angle
double roll = 0;
double pitch = 0;
double yaw = 0;
double roll1 = 0;
double pitch1 = 0;
double yaw1 = 0;
double roll2 = 0;
double pitch2 = 0;
double yaw2 = 0;

float desired_z = 0;
float bias = 0;
float x_in = 0;
float y_in = 0;

// time
double time_now = 0;
double time_start = 0;
double time_temp = 0;
double time_all = 0;
double time_dur = 0;

double time_part_s = 0;
double time_part_f = 0;
double time_part_e = 0;

double time_vio;
double time_vrpn;

struct XYZyaw pose;
struct XYZyaw goal;
struct XYZyaw err;
struct XYZyaw err_l;
struct XYZyaw acc; // command
struct XYZyaw vel;
struct XYZyaw pose_bias;

struct PID pid_ver;
struct PID pid_yaw;
struct PID pid_x;
struct PID pid_y;

void timeDuration(double &start, double &end, double &dur)
{
  start = ros::Time::now().toSec();
  dur = start - end;
  end = start;
}

void pid_compute(struct PID &pid, float &r, float &e, float &e_l, float c){
  pid.PR = e;
  pid.IN += e;
  pid.DE = e - e_l;
  confine(pid.IN, c);
  r = (pid.PR * pid.KP + pid.IN * pid.KI + pid.DE * pid.KD);
}

void keyboard_control()
{
  int c = getch();
  //ROS_INFO("C: %d",c);
  if (c != EOF) {
    switch (c)
    {
      case 111:     // key up  o
      goal.z += 0.1;
      //ROS_INFO("z+!!!");
      break;
      case 112:     // key down p
      goal.z -= 0.1;
      //ROS_INFO("z-!!!");
      break;
      case 119:     // key foward  w
      goal.x += 0.05;
      break;
      case 115:     // key back   s
      goal.x -= 0.05;
      break;
      case 97:      // key left    a
      goal.y += 0.05;
      break;
      case 100:     // key right   d
      goal.y -= 0.05;
      break;
      case 114:     // key return  r
      goal.x = 0;
      goal.y = 0;
      goal.z = 0;
      goal.yaw = 0;
      break;
      case 105:    // i
      goal.yaw -= 0.1;
      break;
      case 117:    // u
      goal.yaw += 0.1;
      break;
      case 52:
      ROS_INFO("Prepare to landing");
      takeoff_land(dji_sdk::DroneTaskControl::Request::TASK_LAND);
      break;
    }
  }
}

void status_cb(const std_msgs::UInt8::ConstPtr& msg)
{
  flight_status = msg->data;
}

void dji_battery_cb(const sensor_msgs::BatteryState::ConstPtr& msg)
{
  battery = *msg;
}

// declare callback function
void mocap_cb(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  mocap_pos = *msg;
//  tf::Quaternion orientation;
//  tf::quaternionMsgToTF(local_position.pose.orientation, orientation);
//  tf::Matrix3x3(orientation).getRPY(roll, pitch, yaw);
  time_vrpn = mocap_pos.header.stamp.sec;

//  world_pos.x = mocap_pos.pose.position.x;
//  world_pos.y = mocap_pos.pose.position.y;
//  world_pos.z = mocap_pos.pose.position.z;
}

void vio_cb(const nav_msgs::Odometry::ConstPtr& msg)
{
  vio_msgs = *msg;
  tf::Quaternion vio_w_b;
  tf::quaternionMsgToTF(vio_msgs.pose.pose.orientation, vio_w_b);
  tf::Matrix3x3(vio_w_b).getRPY(roll, pitch, yaw);
  pose.x = vio_msgs.pose.pose.position.x;
  pose.y = vio_msgs.pose.pose.position.y;
  pose.z = vio_msgs.pose.pose.position.z;
  vel.x = vio_msgs.twist.twist.linear.x;
  vel.y = vio_msgs.twist.twist.linear.y;
  vel.z = vio_msgs.twist.twist.linear.z;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ncrl_vio_offb");
  ros::NodeHandle nh;

  // subscriber
  sub_flight_status = nh.subscribe<std_msgs::UInt8>             ("dji_sdk/flight_status",2, &status_cb);
  sub_dji_battery   = nh.subscribe<sensor_msgs::BatteryState>   ("dji_sdk/battery_state",2, &dji_battery_cb);
  sub_vio           = nh.subscribe<nav_msgs::Odometry>          ("vio_m100",2, &vio_cb);
  sub_host_mocap    = nh.subscribe<geometry_msgs::PoseStamped>  ("vrpn_client_node/RigidBody5/pose",2, &mocap_cb);

  // publisher
  pub_attitude_ctrl   = nh.advertise<sensor_msgs::Joy>      ("dji_sdk/flight_control_setpoint_generic",2);
  pub_world_position  = nh.advertise<geometry_msgs::Point>  ("ground/position",2);

  // service client
  sdk_ctrl_authority_service = nh.serviceClient<dji_sdk::SDKControlAuthority> ("dji_sdk/sdk_control_authority");
  drone_task_service         = nh.serviceClient<dji_sdk::DroneTaskControl>("dji_sdk/drone_task_control");
  query_version_service      = nh.serviceClient<dji_sdk::QueryDroneVersion>("dji_sdk/query_drone_version");

  obtain_control_result = obtain_control();
  if(obtain_control_result){
    ROS_INFO("obtain control success");
    if(is_M100())
    {
      ROS_INFO("M100 taking off!");
      takeoff_result = M100monitoredTakeoff();
    }
  }
  else{
    ROS_INFO("Prepare to landing");
    takeoff_land(dji_sdk::DroneTaskControl::Request::TASK_LAND);
  }

  // initial
  pid_ver.KP = 0;pid_ver.KI = 0;pid_ver.KD = 0;
  pid_ver.IN = 0;pid_ver.DE = 0;pid_ver.PR = 0;

  pid_yaw.KP = 0;pid_yaw.KI = 0;pid_yaw.KD = 0;
  pid_yaw.IN = 0;pid_yaw.DE = 0;pid_yaw.PR = 0;

  pid_x.KP = 0;pid_x.KI = 0;pid_x.KD = 0;
  pid_x.PR = 0;pid_x.IN = 0;pid_x.DE = 0;

  pid_y.KP = 0;pid_y.KI = 0;pid_y.KD = 0;
  pid_y.PR = 0;pid_y.IN = 0;pid_y.DE = 0;

  goal.x = 0;goal.y = 0;goal.z = 0;goal.yaw = 0;

  // get parameter from launch file
  ros::param::get("~ver_KP",pid_ver.KP);
  ros::param::get("~ver_KI",pid_ver.KI);
  ros::param::get("~ver_KD",pid_ver.KD);

  ros::param::get("~X_KP",pid_x.KP);
  ros::param::get("~X_KI",pid_x.KI);
  ros::param::get("~X_KD",pid_x.KD);

  ros::param::get("~Y_KP",pid_y.KP);
  ros::param::get("~Y_KI",pid_y.KI);
  ros::param::get("~Y_KD",pid_y.KD);

  ros::param::get("~yaw_KP",pid_yaw.KP);
  ros::param::get("~yaw_KI",pid_yaw.KI);
  ros::param::get("~yaw_KD",pid_yaw.KD);

  ros::param::get("~desired_height",desired_z);
  ros::param::get("~bias",bias);

  ros::Rate r(100);
  int count = 0;
  while(ros::ok()){
    if(obtain_control_result && takeoff_result){

      // obtain control
      if(obtain_control_result == obtain_control()){
        obtain_control_result = true;
      }
      else{
        obtain_control_result = false;
        ROS_INFO("Prepare to landing");
        takeoff_land(dji_sdk::DroneTaskControl::Request::TASK_LAND);
        break;
      }

      // time compute
      time_now = ros::Time::now().toSec();
      if (count == 0){
        time_start  = time_now;
      }
      else{
        time_dur  = time_now - time_temp;
        time_all  = time_now - time_start;
      }
      time_temp = time_now;

      keyboard_control();

      printf("time : %f \t all : %f sec\n",time_dur,time_all);
      // printf message to debug
      //printf("euler angle : %f \t %f \t %f \t\n",roll,pitch,yaw);
      printf("vio and mocap time err : %f\n",fabs(time_vrpn - time_vio));
      printf("bias : %f\n",bias);
      printf("World pos : %f \t %f \t %f \n",pose.x,pose.y,pose.z);
      printf("goal pos : %f \t %f \t %f \n",goal.x,goal.y,goal.z);
      //printf("bias pos : %f \t %f \t %f \n",pose_bias.x,pose_bias.y,pose_bias.z);
      //printf("battery : %f\n",battery.percentage);

      err.x = goal.x - pose.x;
      err.y = goal.y - pose.y;
      err.z = goal.z + desired_z - pose.z;
      err.yaw = goal.yaw - yaw;

      printf("err(b) : %f \t %f \t %f \t %f \n",err.x,err.y,err.z,err.yaw);

      pid_compute(pid_x, acc.x, err.x, err_l.x, 20);
      pid_compute(pid_y, acc.y, err.y, err_l.y, 20);
      pid_compute(pid_ver, thrust_cmd, err.z, err_l.z, 30);
      pid_compute(pid_yaw, yaw_cmd, err.yaw, err_l.yaw, 1);

      rotZ(acc.x, acc.y, -yaw);
      thrust_cmd += bias;

      if (count <= 500 || time_all <= 5){
        confine(thrust_cmd,60);
      }

      // handle desire roll and pitch
      roll_cmd = -atan2(acc.y,GRAVITY);
      pitch_cmd = atan2(acc.x,GRAVITY);
      confine(roll_cmd,0.5);
      confine(pitch_cmd,0.5);

      count += 1;
      err_l = err;
      // publish cmd
      printf("cmd : %f \t %f \t %f \t %f\n",RAD2DEG(roll_cmd),RAD2DEG(pitch_cmd),thrust_cmd,RAD2DEG(yaw_cmd));

      sensor_msgs::Joy controldata;
      controldata.axes.push_back(roll_cmd);
      controldata.axes.push_back(pitch_cmd);
      controldata.axes.push_back(thrust_cmd);
      controldata.axes.push_back(yaw_cmd);
      controldata.axes.push_back(flag);
      pub_attitude_ctrl.publish(controldata);

      world_pos.x = pose.x;
      world_pos.y = pose.y;
      world_pos.z = pose.z;
      pub_world_position.publish(world_pos);
      printf("----------------------------------------------\n");
    }
    ros::spinOnce();
    r.sleep();
  }
}
