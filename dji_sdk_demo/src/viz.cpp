#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <sensor_msgs/Imu.h>
#include "std_msgs/Header.h"
//#include <Eigen/Dense>
#include <visualization_msgs/Marker.h>
#include <eigen3/Eigen/Dense>
#include <cmath>
#include <geometry_msgs/Point.h>
#include <nav_msgs/Odometry.h>
//using namespace Eigen;
using namespace std;
float dt=0.005;
float wx,wy,wz;
float ax=0,ay=0,az=0;
float abx=0,aby=0,abz=0;
float rollt,pitcht,yawt;
float x[20000],y[20000],z[20000];
float x_p[20000],y_p[20000],z_p[20000];
int a=0, b=0, a1 = 0;
float roll,pitch,yaw;
bool sett=false;
geometry_msgs::Vector3 output_pose;
geometry_msgs::Point plot;
void imu(const sensor_msgs::Imu msg)
{
	plot.x = msg.linear_acceleration.x;
	plot.y = msg.linear_acceleration.y;
	plot.z = msg.linear_acceleration.z;
	/*plot.x = msg.angular_velocity.x;
	plot.y = msg.angular_velocity.y;
	plot.z = msg.angular_velocity.z;*/
}
void Callback(const geometry_msgs::PoseStamped msg)
{
	x[a]=msg.pose.position.x;
	y[a]=msg.pose.position.y;
	z[a]=msg.pose.position.z;
	//ROS_INFO("x =  %.4f", x[a]);
	a++;

}
/*void output_cb(const geometry_msgs::Vector3::ConstPtr &msg)
{
	output_pose = *msg;
	x_p[a1] = output_pose.x;
	y_p[a1] = output_pose.y;
	z_p[a1] = output_pose.z;
	a1++;
}*/
geometry_msgs::Vector3 s_old, s;
void vision(const nav_msgs::Odometry msg)
{
	/*x_p[a1] = msg.pose.pose.position.y-y_p[0];
	y_p[a1] = -msg.pose.pose.position.x-x_p[0];
	z_p[a1] = msg.pose.pose.position.z-z_p[0];*/
	if(sett==false){
	s.x=msg.pose.pose.position.x;
	s.y=msg.pose.pose.position.y;
	s.z=msg.pose.pose.position.z;
	sett=true;
	}
	x_p[a1] = (msg.pose.pose.position.y-s.y);
	y_p[a1] = -(msg.pose.pose.position.x-s.x);
	z_p[a1] = msg.pose.pose.position.z-s.z+0.12;
	ROS_INFO("org= %.4f,%.4f,%.4f", s.y,-s.x,s.z);
	a1++;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "viz");

  ros::NodeHandle nh;

  //ros::Publisher pub = nh.advertise<geometry_msgs::PoseStamped>("/pose", 10);

	//ros::Subscriber sub = nh.subscribe<geometry_msgs::PoseStamped>("/mavros/local_position/pose", 1, Callback);
  ros::Subscriber sub = nh.subscribe<geometry_msgs::PoseStamped>("/vrpn_client_node/RigidBody1/pose", 1, Callback);
	ros::Subscriber vsub = nh.subscribe<nav_msgs::Odometry>("/vins_estimator/odometry", 1, vision);
	//ros::Subscriber psub = nh.subscribe<geometry_msgs::Vector3>("/output_pose", 1, output_cb);
	ros::Subscriber isub = nh.subscribe<sensor_msgs::Imu>("/camera/imu/data_raw", 1, imu);
	//ros::Subscriber isub = nh.subscribe<sensor_msgs::Imu>("/mavros/imu/data_raw", 1, imu);
	ros::Publisher marker_pub = nh.advertise<visualization_msgs::Marker>("Groundtruth", 1);
	ros::Publisher marker2_pub = nh.advertise<visualization_msgs::Marker>("VIO", 1);
	ros::Publisher pub = nh.advertise<geometry_msgs::Point>("/plot", 10);

  ros::Rate loop_rate(100);
  x[0] = 0;
  z[0] = 0;

  while (ros::ok())
  {
	pub.publish(plot);
	visualization_msgs::Marker points, line_strip, line_list;
	visualization_msgs::Marker points2, line_strip2, line_list2;
    points.header.frame_id = line_strip.header.frame_id = line_list.header.frame_id = "/map";
    points2.header.frame_id = line_strip2.header.frame_id = line_list2.header.frame_id = "/map";
    points.header.stamp = line_strip.header.stamp = line_list.header.stamp = ros::Time::now();
    points2.header.stamp = line_strip2.header.stamp = line_list2.header.stamp = ros::Time::now();
    points.ns = line_strip.ns = line_list.ns = "points_and_lines";
    points2.ns = line_strip2.ns = line_list2.ns = "points_and_lines";
    points.action = line_strip.action = line_list.action = visualization_msgs::Marker::ADD;
    points2.action = line_strip2.action = line_list2.action = visualization_msgs::Marker::ADD;
    points.pose.orientation.w = line_strip.pose.orientation.w = 1.0;
    points2.pose.orientation.w = line_strip2.pose.orientation.w = 1.0;

    points.id = 0;
    line_strip.id = 1;
    line_list.id = 2;

    points2.id = 3;
    line_strip2.id = 4;
    line_list2.id = 5;

    points.type = visualization_msgs::Marker::POINTS;
    line_strip.type = visualization_msgs::Marker::LINE_STRIP;
    line_list.type = visualization_msgs::Marker::LINE_LIST;

    points2.type = visualization_msgs::Marker::POINTS;
    line_strip2.type = visualization_msgs::Marker::LINE_STRIP;
    line_list2.type = visualization_msgs::Marker::LINE_LIST;

    points.scale.x = 0.01;
    points.scale.y = 0.01;
    line_strip.scale.x = 0.1;
    points.color.r = 1.0f;
    points.color.a = 1.0;
    line_strip.color.g = 1.0;
    line_strip.color.a = 1.0;


	points2.scale.x = 0.05;
    points2.scale.y = 0.05;
	line_strip2.scale.x = 0.1;
    points2.color.g = 1.0f;
    points2.color.a = 1.0;
    line_strip2.color.b = 1.0;
    line_strip2.color.a = 1.0;
		
	for(int i=0;i<20000;i++)
	{
    geometry_msgs::Point p, p1;
			
    p.x = x[i];
    p.y = y[i];
    p.z = z[i];

    p1.x = x_p[i];
    p1.y = y_p[i];
    p1.z = z_p[i];
    points.points.push_back(p);
    points2.points.push_back(p1);
    //line_strip.points.push_back(p);
	}
		
    //pub.publish(msg);
	marker_pub.publish(points);
	marker_pub.publish(line_strip);

	marker2_pub.publish(points2);
	//marker2_pub.publish(line_strip2);
    loop_rate.sleep();
    ros::spinOnce();
	
  }


  return 0;
}
