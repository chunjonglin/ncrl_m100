/*
 * Target : Use attitude, mocap, PID to control Matrice 100
 */
// ros include
#include <ros/ros.h>
#include <cmath>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PointStamped.h>
#include <tf/tf.h>
// dji include
#include <dji_sdk/dji_sdk.h>
#include <dji_sdk/dji_sdk_node.h>

#define GRAVITY 9.81

// declare subscriber
ros::Subscriber sub_host_mocap;
ros::Subscriber sub_flight_status;
ros::Subscriber sub_dji_battery;
// declare publisher
ros::Publisher pub_attitude_ctrl;

// declare service client
ros::ServiceClient sdk_ctrl_authority_service;
ros::ServiceClient drone_task_service;
ros::ServiceClient query_version_service;

// declare msgs
geometry_msgs::PoseStamped local_position;
geometry_msgs::Vector3 pose;
geometry_msgs::Vector3 goal;
geometry_msgs::Vector3 err;
geometry_msgs::Vector3 acc;
sensor_msgs::BatteryState battery;

uint8_t flight_status = 255;
float goal_yaw = 0;
float err_yaw;
double roll, pitch, yaw;
float roll_cmd, pitch_cmd, thrust_cmd, yaw_cmd;
float desired_z;
float bias;
float bias_battery;
int count = 0;
int stamp = 0;
int stamp_last = 0;

// test
float x_in;
float y_in;

// declare detect delay mocap
int delay_a = 0; // delay all
int delay_c = 0; // delay continous
int delay_t = 0; // delay temp
int mocap_delay = 0;
// declare flight control flag
// VERTICAL_THRUST = 0~100%
// HORIZONTAL_ANGLE = limit 0.611 rad
// YAW_ANGLE = limit -pi to pi
// body frame
uint8_t flag = (
                DJISDK::VERTICAL_THRUST      |
                DJISDK::HORIZONTAL_ANGLE     |
                DJISDK::YAW_ANGLE            |
                DJISDK::HORIZONTAL_BODY      |
                DJISDK::STABLE_ENABLE
               );

struct PID{
  float KP;float KI;float KD;
  float PR;float IN;float DE;
};

struct PID pid_ver;
struct PID pid_hor_roll;
struct PID pid_hor_pitch;
struct PID pid_yaw;
struct PID pid_x;
struct PID pid_y;

char getch(){
    int flags = fcntl(0, F_GETFL, 0);
    fcntl(0, F_SETFL, flags | O_NONBLOCK);

    char buf = 0;
    struct termios old = {0};
    if (tcgetattr(0, &old) < 0) {
        perror("tcsetattr()");
    }
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0) {
        perror("tcsetattr ICANON");
    }
    if (read(0, &buf, 1) < 0) {
        //perror ("read()");
    }
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0) {
        perror ("tcsetattr ~ICANON");
    }
    return (buf);
}

void keyboard_control()
{
  int c = getch();
  //ROS_INFO("C: %d",c);
  if (c != EOF) {
    switch (c)
    {
      case 111:     // key up  o
      goal.z += 0.01;
      //ROS_INFO("z+!!!");
      break;
      case 112:     // key down p
      goal.z -= 0.1;
      //ROS_INFO("z-!!!");
      break;
      case 119:     // key foward  w
      goal.x += 0.05;
      break;
      case 115:     // key back   s
      goal.x -= 0.05;
      break;
      case 97:      // key left    a
      goal.y += 0.05;
      break;
      case 100:     // key right   d
      goal.y -= 0.05;
      break;
      case 114:     // key return  r
      goal.x = 0;
      goal.y = 0;
      goal.z = 0;
      goal_yaw = 0;
      break;
      case 105:    // i
      goal_yaw -= 0.1;
      break;
      case 117:    // u
      goal_yaw += 0.1;
      break;
    }
  }
}

void confine(float &data, float threshold)
{
  if(data >= threshold || data <= -threshold){
    if(data >= threshold)
      data = threshold;
    else
      data = -threshold;
  }
  else{
    data = data;
  }
}

bool obtain_control()
{
  dji_sdk::SDKControlAuthority authority;
  authority.request.control_enable=1;
  sdk_ctrl_authority_service.call(authority);

  if(!authority.response.result)
  {
    ROS_ERROR("obtain control failed!");
    return false;
  }

  return true;
}

bool takeoff_land(int task)
{
  dji_sdk::DroneTaskControl droneTaskControl;

  droneTaskControl.request.task = task;

  drone_task_service.call(droneTaskControl);

  if(!droneTaskControl.response.result)
  {
    ROS_ERROR("takeoff_land fail");
    return false;
  }

  return true;
}

bool is_M100()
{
  dji_sdk::QueryDroneVersion query;
  query_version_service.call(query);

  if(query.response.version == DJISDK::DroneFirmwareVersion::M100_31)
  {
    return true;
  }
  return false;
}

bool
M100monitoredTakeoff()
{
  ros::Time start_time = ros::Time::now();

  if(!takeoff_land(dji_sdk::DroneTaskControl::Request::TASK_TAKEOFF)){
    return false;
  }

  ros::Duration(0.01).sleep();
  ros::spinOnce();

  // Step 1: If M100 is not in the air after 10 seconds, fail.
  while (ros::Time::now() - start_time < ros::Duration(10)){
    ros::Duration(0.01).sleep();
    ros::spinOnce();
  }

  if(flight_status != DJISDK::M100FlightStatus::M100_STATUS_IN_AIR)
  {
    ROS_ERROR("Takeoff failed.");
    return false;
  }
  else
  {
    start_time = ros::Time::now();
    ROS_INFO("Successful takeoff!");
    ros::spinOnce();
  }
  return true;
}

// declare callback function
void mocap_cb(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  local_position = *msg;
  tf::Quaternion orientation;
  tf::quaternionMsgToTF(local_position.pose.orientation, orientation);
  tf::Matrix3x3(orientation).getRPY(roll, pitch, yaw);
  stamp = local_position.header.seq;

  pose.x = local_position.pose.position.x;
  pose.y = local_position.pose.position.y;
  pose.z = local_position.pose.position.z;
}

void status_cb(const std_msgs::UInt8::ConstPtr& msg)
{
  flight_status = msg->data;
}

void dji_battery_cb(const sensor_msgs::BatteryState::ConstPtr& msg)
{
  battery = *msg;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ncrl_vrpn_offb");
  ros::NodeHandle nh;

  // subscriber
  sub_host_mocap    = nh.subscribe<geometry_msgs::PoseStamped> ("vrpn_client_node/RigidBody1/pose",2, &mocap_cb);
  sub_flight_status = nh.subscribe<std_msgs::UInt8> ("dji_sdk/flight_status",2, &status_cb);
  sub_dji_battery   = nh.subscribe<sensor_msgs::BatteryState> ("dji_sdk/battery_state",2, &dji_battery_cb);
  // publisher
  pub_attitude_ctrl = nh.advertise<sensor_msgs::Joy> ("dji_sdk/flight_control_setpoint_generic",2);

  // service client
  sdk_ctrl_authority_service = nh.serviceClient<dji_sdk::SDKControlAuthority> ("dji_sdk/sdk_control_authority");
  drone_task_service         = nh.serviceClient<dji_sdk::DroneTaskControl>("dji_sdk/drone_task_control");
  query_version_service      = nh.serviceClient<dji_sdk::QueryDroneVersion>("dji_sdk/query_drone_version");

  // 4 thread for subscriber
  //ros::AsyncSpinner spinner(4);
  //spinner.start();

  bool obtain_control_result = obtain_control();
  bool takeoff_result;

  // obtain control then takeoff
  if(obtain_control_result){
    ROS_INFO("obtain control success");
    if(is_M100())
    {
      ROS_INFO("M100 taking off!");
      takeoff_result = M100monitoredTakeoff();
    }
  }

/////////////////////////////////////////////////////////////////////////////
  // initial
  pid_ver.KP = 0;pid_ver.KI = 0;pid_ver.KD = 0;
  pid_ver.IN = 0;pid_ver.DE = 0;pid_ver.PR = 0;

  pid_hor_roll.KP = 0;pid_hor_roll.KI = 0;pid_hor_roll.KD = 0;
  pid_hor_roll.IN = 0;pid_hor_roll.DE = 0;pid_hor_roll.PR = 0;

  pid_hor_pitch.KP = 0;pid_hor_pitch.KI = 0;pid_hor_pitch.KD = 0;
  pid_hor_pitch.IN = 0;pid_hor_pitch.DE = 0;pid_hor_pitch.PR = 0;

  pid_yaw.KP = 0;pid_yaw.KI = 0;pid_yaw.KD = 0;
  pid_yaw.IN = 0;pid_yaw.DE = 0;pid_yaw.PR = 0;

  goal.x = 0;goal.y = 0;goal.z = 0;
  /////////////////////////////////////////////////////////////////////////////

  // get parameter from launch file
  ros::param::get("~ver_KP",pid_ver.KP);
  ros::param::get("~ver_KI",pid_ver.KI);
  ros::param::get("~ver_KD",pid_ver.KD);

  ros::param::get("~X_KP",pid_x.KP);
  ros::param::get("~X_KI",pid_x.KI);
  ros::param::get("~X_KD",pid_x.KD);

  ros::param::get("~Y_KP",pid_y.KP);
  ros::param::get("~Y_KI",pid_y.KI);
  ros::param::get("~Y_KD",pid_y.KD);

  ros::param::get("~yaw_KP",pid_yaw.KP);
  ros::param::get("~yaw_KI",pid_yaw.KI);
  ros::param::get("~yaw_KD",pid_yaw.KD);

  ros::param::get("~desired_height",desired_z);
  ros::param::get("~bias",bias);
  ros::param::get("~mocap_delay",mocap_delay);

  ros::Rate r(100);
  while(ros::ok()){
    if(obtain_control_result && takeoff_result )//&& mocap_receive)
    {
      if(obtain_control_result = obtain_control()){
        printf("obtain control");
    //    c++;
      }

      printf("\n");

      /////////////////////////////////////////
      // check mocap whether delay
      if (stamp_last == stamp){
        ROS_WARN("MOCAP DELAY");
        delay_a++;
        delay_t++;
        if (delay_t > delay_c){
          delay_c = delay_t;
        }
      }
      else{
        delay_t = 0;
      }
      printf("total delay : %d\n",delay_a);
      printf("continous delay : %d\n",delay_c);
      if (delay_c >= mocap_delay){
        float delay_time = mocap_delay;
        delay_time = delay_time/100;
        ROS_WARN("dalay exceed %f sec",delay_time);
        ROS_INFO("Prepare to landing");
        takeoff_land(dji_sdk::DroneTaskControl::Request::TASK_LAND);
        break;
      }

      stamp_last = stamp;
      /////////////////////////////////////////

      printf("roll : %f \t pitch : %f \t yaw : %f\n",RAD2DEG(roll),RAD2DEG(pitch),RAD2DEG(yaw));
      printf("count : %d\n",count);
      printf("stamp : %d\n",stamp);
      printf("battery : %f\n",battery.percentage);
      printf("mocap x : %f \t y : %f \t z : %f\n",pose.x,pose.y,pose.z);
      // adjust goal point
      keyboard_control();

      // inertia frame
      err.x = goal.x - pose.x;
      err.y = goal.y - pose.y;
      err.z = goal.z + desired_z - pose.z;
      err_yaw = goal_yaw - yaw;

      printf("goal(in) x : %f \t y : %f \t z : %f \t yaw : %f\n",goal.x, goal.y, goal.z+desired_z, goal_yaw);

      // trans err into bodyframe
      float x1 = err.x;
      float y1 = cos(-roll)*err.y - sin(-roll)*err.z;
      float z1 = sin(-roll)*err.y + cos(-roll)*err.z;

      float x2 = sin(-pitch)*z1 + cos(-pitch)*x1;
      float y2 = y1;
      float z2 = cos(-pitch)*z1 - sin(-pitch)*x1;

      err.x = cos(-yaw)*x2 - sin(-yaw)*y2;
      err.y = sin(-yaw)*x2 + cos(-yaw)*y2;
      err.z = z2;

      printf("err(b) x : %f \t y : %f \t z : %f \t yaw : %f\n",err.x,err.y,err.z,err_yaw);

      float temp = pid_ver.PR;
      pid_ver.PR = err.z;
      pid_ver.IN += err.z;
      pid_ver.DE = err.z - temp;
      printf("vertical PR : %f \t IN : %f \t DE : %f\n",pid_ver.PR,pid_ver.IN,pid_ver.DE);
      confine(pid_ver.IN,30);

      temp = pid_x.PR;
      pid_x.PR = err.x;
      pid_x.IN += err.x;
      pid_x.DE = err.x - temp;
      printf("x PR : %f \t IN : %f \t DE : %f\n",pid_x.PR,pid_x.IN,pid_x.DE);
      confine(pid_x.IN,20);
      x_in += err.x;
      printf("x_in : %f\n",x_in);

      temp = pid_y.PR;
      pid_y.PR = err.y;
      pid_y.IN += err.y;
      pid_y.DE = err.y - temp;
      printf("y PR : %f \t IN : %f \t DE : %f\n",pid_y.PR,pid_y.IN,pid_y.DE);
      confine(pid_y.IN,20);
      y_in += err.y;
      printf("y_in : %f\n",y_in);

      temp = pid_yaw.PR;
      pid_yaw.PR = err_yaw;
      pid_yaw.IN += err_yaw;
      pid_yaw.DE = err_yaw - temp;
      confine(pid_yaw.IN,1);

      // body frame
      acc.x = ( pid_x.PR * pid_x.KP
              + pid_x.IN * pid_x.KI
              + pid_x.DE * pid_x.KD);

      acc.y = ( pid_y.PR * pid_y.KP
              + pid_y.IN * pid_y.KI
              + pid_y.DE * pid_y.KD);

      thrust_cmd = ( pid_ver.PR * pid_ver.KP
                   + pid_ver.IN * pid_ver.KI
                   + pid_ver.DE * pid_ver.KD)+bias;

      yaw_cmd = ( pid_yaw.PR * pid_yaw.KP
                + pid_yaw.IN * pid_yaw.KI
                + pid_yaw.DE * pid_yaw.KD);

      roll_cmd = -atan2(acc.y,GRAVITY);
      pitch_cmd = atan2(acc.x,GRAVITY);
     
      confine(roll_cmd,0.5);
      confine(pitch_cmd,0.5);
     // confine(thrust_cmd,90);
      printf("acc x : %f \t y : %f\n",acc.x, acc.y);
      printf("cmd roll : %f \t pitch %f \t thrust : %f \t yaw : %f\n",roll_cmd,pitch_cmd,thrust_cmd,yaw_cmd);
      printf("----------------------------------------------\n");

      sensor_msgs::Joy controldata;
      controldata.axes.push_back(roll_cmd);
      controldata.axes.push_back(pitch_cmd);
      controldata.axes.push_back(thrust_cmd);
      controldata.axes.push_back(yaw_cmd);
      controldata.axes.push_back(flag);
      pub_attitude_ctrl.publish(controldata);
      count ++;
    }
    ros::spinOnce();
    r.sleep();
  }
}
